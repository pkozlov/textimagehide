Text in image hider script
==========================

Python script for hiding text messages in images

(depends on [pillow](http://python-imaging.github.io), fork of PIL, python image library)

Usage
=====
    ~ $ ./hide.py 'message' 'image' 'output'
    ~ $ ./unhide.py 'image'

Example
=======
    ~ $ ./hide.py 'Hello, World' source.png out.png
    ~ $ ./unhide.py out.png
    Hello, World