#!/usr/bin/env python
# -*- coding: utf8 -*-

from PIL import Image
from itertools import product

def bin_message(message):
    message = bytes(message,'utf-8')
    messagelen = len(message)
    binlen = bin(messagelen)[2:]
    if len(binlen) < 8:
        binlen = '0' * (8 - len(binlen)) + binlen
        
    binmessage = []
    binmessage.append(binlen)
    for x in message:
        part = bin(x)[2:]
        partlen = len(part)
        if (partlen < 8):
            part = '0' * (8 - partlen) + part
        binmessage.append(part)
        
    return ''.join(binmessage)
    

def hide_message(message, imagefile, outfile):
    binmessage = bin_message(message)
    image = Image.open(imagefile)
    pix = image.load()
    sizex, sizey = image.size
    nextindex = product(range(sizex), range(sizey))
    for m in binmessage:
        index = next(nextindex)
        r, g, b, a = pix[index]
        lastbit = bin(b)[-1:]
        if m == '0':
            if lastbit == '1':
                b -= 1
        elif m == '1':
            if lastbit == '0':
                b += 1
        
        pix[index] = r,g,b,a
    
    image.save(outfile)
    
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('message',help='message to hide')
    parser.add_argument('image',help='image filename where hide message')
    parser.add_argument('output',help='output image filename')
    args = parser.parse_args()
    
    hide_message(args.message, args.image, args.output)

