#!/usr/bin/env python
# -*- coding: utf8 -*-

from PIL import Image
from itertools import product

def unhide_message(imagefile):
    image = Image.open(imagefile)
    pix = image.load()
    sizex, sizey = image.size
    nextindex = product(range(sizex), range(sizey))
    
    # find length
    messagelen = 0
    for i in range(7,-1,-1):
        index = next(nextindex)
        b = pix[index][2]
        lastbit = bin(b)[-1:]
        if lastbit=='1':
            messagelen += 2**i
            
    message = []
    for i in range(messagelen):
        part = 0
        for i in range(7,-1,-1):
            index = next(nextindex)
            b = pix[index][2]
            lastbit = bin(b)[-1:]
            if lastbit=='1':
                part += 2**i
        message.append(chr(part))
        
    return ''.join(message)
    
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('image',help='image filename where message is hidden')
    args = parser.parse_args()
    print(unhide_message(args.image))

